from threading import Timer
import time

from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QGraphicsScene, QGraphicsView


class ClockWidget(QWidget):
    angleChanged = pyqtSignal(int)

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)

        lay = QVBoxLayout(self)
        lay.setContentsMargins(0,0,0,0)
        self.setLayout(lay)
        self.setStyleSheet("background:transparent;")

        self._margins = 1

        # self.clock = QLabel()
        # self.clock.setText(str(time()))
        # lay.addWidget(self.clock)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update)
        self.timer.start(1000)

    def paintEvent(self, event):

        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)

        painter.fillRect(event.rect(), self.palette().brush(QPalette.Window))

        self.drawClock(painter)

        painter.end()

    def drawClock(self, painter):

        painter.save()
        painter.translate(self.height()/50, self.height() *5/ 9)
        scale = min((self.width() - self._margins) / 120.0,
                    (self.height() - self._margins) / 20.0)
        painter.scale(scale, scale)

        font = QFont(self.font())
        font.setPixelSize(10)
        metrics = QFontMetricsF(font)

        painter.setFont(font)
        painter.setPen(self.palette().color(QPalette.Shadow))
        painter.drawText(0,0,str(time.asctime()))

        painter.restore()


class ClockWidgetScene(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.clock = ClockWidget()

        self.scene = QGraphicsScene()
        self.scene.addWidget(self.clock)

        self.view = QGraphicsView(self.scene)
        self.view.setStyleSheet("border-width: 0px; border-top:1px solid")
        lay = QVBoxLayout(self)
        lay.setContentsMargins(0,0,0,0)
        lay.addWidget(self.view)
        self.setLayout(lay)

    def paintEvent(self, event):
        self.clock.setFixedSize(self.view.viewport().width(), self.view.viewport().height())
        self.scene.setSceneRect(0,0,self.view.viewport().width(), self.view.viewport().height())

    def stop(self):
        self.clock.timer.stop()