from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSlot, pyqtSignal
from PyQt5.QtWidgets import QWidget, QGraphicsScene, QGraphicsView, QVBoxLayout, QLabel, QPushButton

from CompassLineWidget import CompassLineWidget
from CompassWidget import CompassWidget
from RollPitchWidget import RollPitchWidget


class RollPitchWidgetScene(QWidget):
    pitchChanged = pyqtSignal(int)
    rollChanged = pyqtSignal(int)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.rp = RollPitchWidget()
        self.dataRoll = QLabel('Крен = %03d\u00B0' % 0)
        self.dataRoll.setFont(QtGui.QFont("Times", 16, QtGui.QFont.Bold))
        self.dataRoll.setAlignment(QtCore.Qt.AlignHCenter)
        self.dataPitch = QLabel('Дифферент = %03d\u00B0' % 0)
        self.dataPitch.setFont(QtGui.QFont("Times", 16, QtGui.QFont.Bold))
        self.dataPitch.setAlignment(QtCore.Qt.AlignHCenter)

        self.scene = QGraphicsScene()
        self.scene.addWidget(self.rp)

        self.view = QGraphicsView(self.scene)
        self.view.setStyleSheet("border-width: 0px; border-top:1px solid")
        lay = QVBoxLayout(self)
        lay.setContentsMargins(0,0,0,0)
        lay.addWidget(self.view)
        lay.addWidget(self.dataRoll)
        lay.addWidget(self.dataPitch)
        self.setLayout(lay)

    def paintEvent(self, event):
        self.rp.setFixedSize(self.view.viewport().width(), self.view.viewport().height())
        self.scene.setSceneRect(0,0,self.view.viewport().width(), self.view.viewport().height())

    @pyqtSlot(int)
    def setPitch(self, pitch):
        self.rp.setPitch(pitch)
        pitch = 360 - pitch
        if pitch > 180:
            pitch = pitch - 360
            self.dataPitch.setText('Дифферент = -%03d\u00B0' % abs(pitch))
        else:
            self.dataPitch.setText('Дифферент = +%03d\u00B0' % pitch)


    @pyqtSlot(int)
    def setRoll(self, roll):
        self.rp.setRoll(roll)
        if roll > 180:
            roll = roll - 360
            self.dataRoll.setText('Крен = -%03d\u00B0' % abs(roll))
        else:
            self.dataRoll.setText('Крен = +%03d\u00B0' % roll)
