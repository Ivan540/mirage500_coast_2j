import ast

import serial

import config

class SerialPortClass:
    def __init__(self, main_container, port):
        self.mainContainer = main_container

        self.ser = serial.Serial()
        self.ser.port = port
        self.ser.baudrate = 115200
        self.ser.open()

    def writeSonar(self, data):
        try:
            self.ser.write(data)
        except:
            pass

    def readSonar(self):
        return self.ser.read(config.Sonar.bytes_out)

    def read(self):
        try:
            while True:
                while self.ser.read(1) != b'\xaa':
                    pass
                if self.ser.read(1) == b'\xaa':
                    #print(list(self.ser.read(100)))
                    data = self.ser.read(28)
                    data = list(data)
                    self.mainContainer.controllerClass.control_buffer[20:32] = data[:12]
                    # print(self.mainContainer.controllerClass.control_buffer)
                    break

        except Exception as e:
            print(e)

