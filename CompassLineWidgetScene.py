from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QWidget, QGraphicsScene, QGraphicsView, QVBoxLayout

from CompassLineWidget import CompassLineWidget
from CompassWidget import CompassWidget


class CompassLineWidgetScene(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.com = CompassLineWidget()

        self.scene = QGraphicsScene()
        self.scene.addWidget(self.com)

        self.view = QGraphicsView(self.scene)
        self.view.setStyleSheet("border-width: 0px; border-top:1px solid")
        lay = QVBoxLayout(self)
        lay.setContentsMargins(0,0,0,0)
        lay.addWidget(self.view)
        self.setLayout(lay)

    def paintEvent(self, event):
        self.com.setFixedSize(self.view.viewport().width(), self.view.viewport().height())
        self.scene.setSceneRect(0,0,self.view.viewport().width(), self.view.viewport().height())
