# first file. vehicle.py

vehicle = None

class Vehicle:
    def __init__(self, central_widget, video_layout, depth, weight):
        self.depth = depth
        self.weight = weight
        self.video_layout = video_layout

# Singletone pattern
def get_vehicle():
    global vehicle;
    if vehicle != None:
        return vehicle
    vehicle = Vehicle(123, 456, 789, 987)
    return vehicle

# end of vehicle.py





# second file. regulator.py
from .vehicle import get_vehicle


vehicle = get_vehicle()

print(vehicle.depth)


# end of regulator.py
