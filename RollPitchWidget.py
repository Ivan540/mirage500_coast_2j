import math

from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QWidget, QVBoxLayout


class RollPitchWidget(QWidget):
    pitchChanged = pyqtSignal(int)
    rollChanged = pyqtSignal(int)

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)

        self.setStyleSheet("background:transparent;")

        self._pitch = 0
        self._roll = 0
        self._margins = 10

    def paintEvent(self, event):

        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)

        painter.fillRect(event.rect(), self.palette().brush(QPalette.Window))
        # self.drawMarkings(painter)
        self.drawArcs(painter)

        painter.end()

    def drawArcs(self, painter):
        # if self._pitch > 90:
        #     self._pitch = 90
        # if self._pitch < -90:
        #     self._pitch = -90

        painter.save()
        painter.translate(self.width() / 2, self.height() / 2)
        scale = min((self.width() - self._margins) / 120.0,
                    (self.height() - self._margins) / 120.0)
        painter.scale(scale, scale)

        painter.rotate(self._roll)
        painter.setPen(QPen(Qt.NoPen))
        radius = 60

        if self._pitch > 180:
            pitch = self._pitch - 360
        else:
            pitch = self._pitch

        painter.setBrush(QColor(0xA52A2A))
        path = QtGui.QPainterPath()
        path.moveTo(-math.cos(math.radians(pitch)) * radius, -math.sin(math.radians(pitch)) * radius)
        path.arcTo(-radius, -radius, 2 * radius, 2 * radius, pitch, -180 - 2 * pitch)
        path.closeSubpath()
        painter.drawPath(path)

        painter.setBrush(Qt.blue)
        path = QtGui.QPainterPath()
        path.moveTo(-math.cos(math.radians(pitch)) * radius, -math.sin(math.radians(pitch)) * radius)
        path.arcTo(-radius, -radius, 2 * radius, 2 * radius, -180 - pitch, -180 + 2 * pitch)
        path.closeSubpath()
        painter.drawPath(path)

        painter.restore()

    def pitch(self):
        return self._pitch

    @pyqtSlot(int)
    def setPitch(self, pitch):

        if pitch != self._pitch:
            self._pitch = pitch
            self.pitchChanged.emit(pitch)
            self.update()

    pitch = pyqtProperty(float, pitch, setPitch)

    def roll(self):
        return self._roll

    @pyqtSlot(int)
    def setRoll(self, roll):

        if roll != self._roll:
            self._roll = roll
            self.rollChanged.emit(roll)
            self.update()

    roll = pyqtProperty(float, roll, setRoll)
