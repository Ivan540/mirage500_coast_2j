import select
import selectors 
import time
import math
import MainContainer
from copy import copy
# from dataclasses import dataclass
from threading import Thread
import evdev
from evdev import ecodes, ff, UInput, InputEvent, categorize
from numpy.core._multiarray_umath import sign

import DataClass

from simple_pid import PID
#Пи-регулятор для автоглубины
pid = PID(15, 0.1, 0.05, setpoint=0)
pid_yaw = PID(2, 0.2, 0.05, setpoint=0) #(3, 0.1, 0.05, setpoint=0)
pid.sample_time = 0.01  # update every 0.01 seconds
pid_yaw.sample_time = 0.01



# @dataclass
class ControllerState:
    def __init__(self):
        self.parameters = {'autoDepth': [0, (("Автоглубина выкл.", "green", 0),
                                             ("Автоглубина вкл.", "red", 1))],
                           'autoHeading': [0, (("Автоход выкл.", "green", 0),
                                               ("Автоход вкл.", "red", 1))],
                           'laser': [0, (("Лазер выкл.", "green", 0),
                                         ("Лазер вкл.", "red", 1))],
                           'tiltCamera1': [0, (("Tilt camera 1", "black", 0),
                                               ("Tilt camera 1 +", "red", 1),
                                               ("Tilt camera 1 -", "red", 2))],
                           'tiltSonar': [0, (("Tilt sonar", "black", 0),
                                             ("Tilt sonar +", "red", 1),
                                             ("Tilt sonar -", "red", 2))],
                           'focusCamera1': [0, (("Фокус камеры 1", "black", 0),
                                                ("Фокус камеры 1 +", "red", 1),
                                                ("Фокус камеры 1 -", "red", 2))],
                           'zoomCamera1': [0, (("Зум камеры 1", "black", 0),
                                               ("Зум камеры 1 +", "red", 1),
                                               ("Зум камеры 1 -", "red", 2))],
                           'focusCamera2': [0, (("Фокус камеры 2", "black", 0),
                                                ("Фокус камеры 2 +", "red", 1),
                                                ("Фокус камеры 2 -", "red", 2))],
                           'tiltCamera2': [0, (("Tilt камеры 2", "black", 0),
                                               ("Tilt камеры 2 +", "red", 1),
                                               ("Tilt камеры 2 -", "red", 2))],
                           'gearMode': [0, (("Малый ход", "green", 5),
                                            ("Средний ход", "yellow", 2),
                                            ("Полный ход", "red", 1))],
                           'cutter': [0, (("Резак", "black", 0),
                                          ("Резак открыть", "black", 1),
                                          ("Резак закрыть", "black", 2),
                                          ("Резак влево", "black", 3),
                                          ("Резак вправо", "black", 4),
                                          ("Резак вперёд", "black", 5),
                                          ("Резак назад", "black", 6))],
                           'catcher': [0, (("Захват", "black", 0),
                                           ("Захват открыть", "black", 1),
                                           ("Захват закрыть", "black", 2),
                                           ("Захват влево", "black", 3),
                                           ("Захват вправо", "black", 4),
                                           ("Захват вперёд", "black", 5),
                                           ("Захват назад", "black", 6))],
                           'joysticks': [0, (("", "black", 0),
                                           ("Joys ERROR", "red", 1))]}
        self.switch = {'autoDepth': [0, ('autoDepth',)],
                       'gearMode': [0, ('gearMode',)],
                       'autoHeading': [0, ('autoHeading',)],
                       'laser': [0, ('laser',)],
                       'options_right': [0, ('tiltCamera1', 'focusCamera1', 'focusCamera2')],
                       'options_left': [0, ('tiltSonar', 'zoomCamera1', 'tiltCamera2')],
                       'cutter': [0, ('cutter',)],
                       'catcher': [0, ('catcher',)],
                       'joysticks': [0, ('joysticks',)]}

    def next(self, switchName):
        self.parameters[self.nameSwitch(switchName)][0] = (self.parameters[self.nameSwitch(switchName)][0] + 1) \
                                                          % len(self.parameters[self.nameSwitch(switchName)][1])

    def color(self, switchName):
        return self.parameters[self.nameSwitch(switchName)][1][self.parameters[self.nameSwitch(switchName)][0]][1]

    def name(self, switchName):
        return self.parameters[self.nameSwitch(switchName)][1][self.parameters[self.nameSwitch(switchName)][0]][0]

    def value(self, switchName):
        return self.parameters[self.nameSwitch(switchName)][1][self.parameters[self.nameSwitch(switchName)][0]][2]

    def nextSwitch(self, switchName):
        self.switch[switchName][0] = (self.switch[switchName][0] + 1) \
                                     % len(self.switch[switchName][1])

    def nameSwitch(self, switchName):
        return self.switch[switchName][1][self.switch[switchName][0]]

    def getAllSwitchNames(self):
        return self.switch.keys()

    def getValuesLength(self, switchName):
        return len(self.parameters[self.nameSwitch(switchName)][1])

    def setValue(self, switchName, value):
        self.parameters[self.nameSwitch(switchName)][0] = value


class ControllerClass:
    def __init__(self, mainContainer):

        self.stopped = False
        self.mainContainer = mainContainer
        self.control_buffer = [150, 170, 190] + [0] * (32 - 3)
        self.autoDepthCur = 0
        self.left_motor = [0, 0, 0]
        self.right_motor = [0, 0, 0]
        self.hlf_motor = [0, 0, 0]
        self.hrf_motor = [0, 0, 0]
        self.hlb_motor = [0, 0, 0]
        self.hrb_motor = [0, 0, 0]
        self.ept = 0


        self.gamepad0 = None
        self.gamepad1 = None
        self.selector = None

        self.PS_SONY='Sony Interactive Entertainment Wireless Controller'
        
        self.selector = selectors.DefaultSelector()
        
##        self.LookingForFirstController()
##        self.selector.register(self.gamepad0, selectors.EVENT_READ)
##        
##        self.LookingForSecondController()
##        self.selector.register(self.gamepad1, selectors.EVENT_READ)

        rumble = ff.Rumble(strong_magnitude=0xffff, weak_magnitude=0xffff)
        duration_ms = 1000

        effect = ff.Effect(
            ecodes.FF_RUMBLE, -1, 0,
            ff.Trigger(0, 0),
            ff.Replay(duration_ms, 0),
            ff.EffectType(ff_rumble_effect=rumble)
        )

        # self.rumble = self.gamepad.upload_effect(effect)

        self.waitKeyThread = Thread(target=self.waitKey)
        self.waitKeyThread.start()
        # self.gamepad.write(ecodes.EV_FF, self.rumble, repeat_count)

    def stop(self):
        self.stopped = True
        # self.gamepad.erase_effect(self.rumble)

    # def rumbling(self):
    #     self.gamepad.write(ecodes.EV_FF, self.rumble, 1)

    def LookingForFirstController(self):
        while self.gamepad0 is None:
            devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
            for device in devices:
                if self.PS_SONY == device.name and self.gamepad1 != device:
                    self.gamepad0 = device
                    print('Found First Controller:', self.gamepad0)
                    break
            time.sleep(1)
            if self.gamepad0 is None:
                self.mainContainer.controllerState.setValue('joysticks', 1)
                self.mainContainer.controllerStateSignal.emit('joysticks')

    def LookingForSecondController(self):
        while self.gamepad1 is None and self.gamepad0:
            devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
            for device in devices: 
                if self.PS_SONY == device.name and self.gamepad0 != device:
                    self.gamepad1 = device
                    print('Found Second Controller:', self.gamepad1)
                    break
            if self.gamepad1 is None:
                self.mainContainer.controllerState.setValue('joysticks', 1)
                self.mainContainer.controllerStateSignal.emit('joysticks')
        
    def waitKey(self):
        a=0
        b=0
        AUTO_DEPTH=0
        AUTODEPTH=0
        AUTO_HEAD=0
        AUTOHEAD=0
        depth=0
        yaw=0
        yaw_pid = 0
        self.control_buffer[16]=0
        self.LookingForFirstController()
        self.selector.register(self.gamepad0, selectors.EVENT_READ)
        
        self.LookingForSecondController()
        self.selector.register(self.gamepad1, selectors.EVENT_READ)

        while not self.stopped:
            last_controll_buffer = copy(self.control_buffer)
            self.mainContainer.controllerState.setValue('joysticks', 0)
            self.mainContainer.controllerStateSignal.emit('joysticks')
            for key, mask in self.selector.select():
                device = key.fileobj
                if device.fileno() == list(self.selector.get_map().keys())[0]:
                  try:
                    for event in device.read():
##                        print(categorize(event))
                       # if event.code not in [3, 4, 1,0]:
##                        if event.code not in [304, 308, 1, 4, 3, 318, 317, 308, 315]:
                            # print(event, device)

                        #if event.code not in [304, 308, 1, 4, 3, 318, 317, 308, 315]:
                            # continue
                        #self.control_buffer[4]=0

                        if event.code == 315 and event.type == 1 and event.value == 1:
                            if b==0:
                                b=1
                            else:
                                if b==1:
                                    b=2
                                else:
                                    b=0
                            self.mainContainer.controllerState.nextSwitch('options_right')
                            self.mainContainer.controllerState.nextSwitch('options_left')
                            self.mainContainer.controllerStateSignal.emit('options_right')
                            self.mainContainer.controllerStateSignal.emit('options_left')



                        if event.code == 311 and event.type == 1 and event.value == 1:   
                            self.mainContainer.controllerState.setValue('options_right', 1)
                            self.mainContainer.controllerStateSignal.emit('options_right')
                            if b == 0:
                                self.control_buffer[4]|=0x01#тилт камеры +
                            if b == 1:
                                self.control_buffer[16]|=0x01#фокус камеры1+
                            if b == 2:
                                self.control_buffer[16]|=0x10#фокус камеры2+

                                
                        if event.code == 313 and event.type == 1 and event.value == 1:
                            self.mainContainer.controllerState.setValue('options_right', 2)
                            self.mainContainer.controllerStateSignal.emit('options_right')
                            if b == 0:
                                self.control_buffer[4]|=0x02#тилт камеры -
                            if b == 1:
                                self.control_buffer[16]|=0x02#фокус камеры1-
                            if b == 2:
                                self.control_buffer[16]|=0x20#фокус камеры2-
                        if (event.code == 311 or event.code == 313) and event.type == 1 and event.value == 0:
                            self.mainContainer.controllerState.setValue('options_right', 0)
                            self.mainContainer.controllerStateSignal.emit('options_right')
                            
                            self.control_buffer[4]&=0xfc
                            self.control_buffer[16]&=0xfc
                            self.control_buffer[16]&=0xcf

                        if event.code == 310 and event.type == 1 and event.value == 1:
                            if b==0:
                                self.control_buffer[4]|=0x04#тилт сонара+
                            if b == 1:
                                self.control_buffer[16]|=0x04#зум камеры1+
                            if b == 2:
                                self.control_buffer[16]|=0x40#тилт камеры2+
                                
                            self.mainContainer.controllerState.setValue('options_left', 1)
                            self.mainContainer.controllerStateSignal.emit('options_left')
                        if event.code == 312 and event.type == 1 and event.value == 1:
                            if b==0:
                                self.control_buffer[4]|=0x08#тилт сонара-
                            if b == 1:
                                self.control_buffer[16]|=0x08#зум камеры1-
                            if b == 2:
                                self.control_buffer[16]|=0x80#тилт камеры2-
                            self.mainContainer.controllerState.setValue('options_left', 2)
                            self.mainContainer.controllerStateSignal.emit('options_left')
                        if (event.code == 310 or event.code == 312) and event.type == 1 and event.value == 0:
                            
                            self.control_buffer[4]&=0xf3
                            self.control_buffer[16]&=0xf3
                            self.control_buffer[16]&=0x3f
                            
                            self.mainContainer.controllerState.setValue('options_left', 0)
                            self.mainContainer.controllerStateSignal.emit('options_left')

                        if event.code == 305 and event.type == 1 and event.value == 1:
                            self.mainContainer.controllerState.next('laser')
                            if a == 1:
                                self.control_buffer[3]&=0xfd
                                a=0
                               # print(self.control_buffer[3])
                            else:
                                a=1
                                self.control_buffer[3]|=0x02
                                print(self.control_buffer[3])
                            self.mainContainer.controllerStateSignal.emit('laser')

                        if event.code == 308 and event.type == 1 and event.value == 1:
                            self.mainContainer.controllerState.next('autoHeading')
                            self.mainContainer.controllerStateSignal.emit('autoHeading')
                            if AUTO_HEAD==0:
                                AUTO_HEAD = 1
                                if self.mainContainer.compass_angle is None:
                                    print('Compass still is not initialized')
                                else:
                                    AUTOHEAD = self.mainContainer.compass_angle
                                    print('AUTOHEAD', AUTOHEAD)
                                
                            else:
                                AUTO_HEAD = 0
                                pid_yaw.auto_mode = False  # no new values will be computed when pid is called
                                self.hlf_motor[1] = 0
                                self.hrf_motor[1] = 0
                                self.hlb_motor[1] = 0
                                self.hrb_motor[1] = 0

                                

                        if event.code == 317 and event.type == 1 and event.value == 1:
                            self.mainContainer.controllerState.next('autoDepth')
                            self.mainContainer.controllerStateSignal.emit('autoDepth')
                            if AUTO_DEPTH==0:
                                AUTO_DEPTH=1
                                
                                if self.mainContainer.depth is None:
                                    print('Depth still is not initialized')
                                else:
                                    
                                    AUTODEPTH = self.mainContainer.depth
                                
                            else:
                                AUTO_DEPTH=0
                                pid.auto_mode = False  # no new values will be computed when pid is called


                        if event.code == 318 and event.type == 1 and event.value == 1:
                            self.mainContainer.controllerState.next('gearMode')
                            self.mainContainer.controllerStateSignal.emit('gearMode')


                        # left/right joy
                        if event.code == 0 and event.type == 3:
                            CONTROL_BYTE = [8, 9]
                            ZERO = 127.5
                            DEAD_ZONE = 27
                            SCALE = ZERO / (ZERO - DEAD_ZONE)

                            power = int((event.value - ZERO))
                            if sign(power) > 0:
                                direction = 0
                            else:
                                direction = 1
                            power = abs(power)

                            if power < DEAD_ZONE and self.left_motor[2] == 0 and self.right_motor[2] == 0:
                                continue
                            if power < DEAD_ZONE:
                                  self.left_motor[2] = 0
                                  self.right_motor[2] = 0
                                  self.hlf_motor[2] = 0
                                  self.hrf_motor[2] = 0
                                  self.hlb_motor[2] = 0
                                  self.hrb_motor[2] = 0
                            else:
                                power = int((power - DEAD_ZONE) * SCALE /
                                            self.mainContainer.controllerState.value('gearMode'))

                                if direction == 0:
                                  self.left_motor[2] = power
                                  self.right_motor[2] = power
                                  self.hlf_motor[2] = power
                                  self.hrf_motor[2] = -power
                                  self.hlb_motor[2] = power
                                  self.hrb_motor[2] = -power
                                    
                                else:
                                  self.left_motor[2] = -power
                                  self.right_motor[2] = -power
                                  self.hlf_motor[2] = -power
                                  self.hrf_motor[2] = power
                                  self.hlb_motor[2] = -power
                                  self.hrb_motor[2] = power


                        # vertical motor
                        if event.code == 1 and event.type == 3:
                            CONTROL_BYTE = 7
                            ZERO = 127.5
                            DEAD_ZONE = 17
                            SCALE = ZERO / (ZERO - DEAD_ZONE)
                            inDeadZone = False
                            last_power = self.control_buffer[CONTROL_BYTE] & 0x7f

                            power = int((event.value - ZERO))
                            if -sign(power) > 0:
                                direction = 1
                            else:
                                direction = 0
                            power = abs(power)

                            if power < DEAD_ZONE and last_power == 0:
                                continue
                            if power < DEAD_ZONE:
                                inDeadZone = True

                            if inDeadZone:
                                direction = 0
                                power = 0
                            else:
                                power = int((power - DEAD_ZONE) * SCALE /
                                            self.mainContainer.controllerState.value('gearMode'))
                            if AUTO_DEPTH==0:

                                self.control_buffer[CONTROL_BYTE] = direction << 7 | power
                                self.control_buffer[12]=self.control_buffer[7]


                        # left/right joy
                        if event.code == 3 and event.type == 3:
                            CONTROL_BYTE = [8, 9]
                            ZERO = 127.5
                            DEAD_ZONE = 27
                            SCALE = ZERO / (ZERO - DEAD_ZONE)
                            # last_power = self.control_buffer[CONTROL_BYTE] & 0x7f

                            power = int((event.value - ZERO))
                            if -sign(power) > 0:
                                direction = 0
                            else:
                                direction = 1
                            power = abs(power)
                            if AUTO_HEAD == 0:
                            
                                if power < DEAD_ZONE and self.left_motor[1] == 0 and self.right_motor[1] == 0:
                                    continue
                                if power < DEAD_ZONE:
                                    self.left_motor[1] = 0
                                    self.right_motor[1] = 0
                                    self.hlf_motor[1] = 0
                                    self.hrf_motor[1] = 0
                                    self.hlb_motor[1] = 0
                                    self.hrb_motor[1] = 0
                                else:
                                    power = int((power - DEAD_ZONE) * SCALE /
                                                self.mainContainer.controllerState.value('gearMode'))
                                    if direction == 0:
                                        self.left_motor[1] = power
                                        self.right_motor[1] = -power
                                        self.hlf_motor[1] = -power
                                        self.hrf_motor[1] = power
                                        self.hlb_motor[1] = power
                                        self.hrb_motor[1] = -power
                                        
                                    else:
                                        self.left_motor[1] = -power
                                        self.right_motor[1] = power
                                        self.hlf_motor[1] = power
                                        self.hrf_motor[1] = -power
                                        self.hlb_motor[1] = -power
                                        self.hrb_motor[1] = power

                            # print('x', power, direction)

                        # forward/backward joy
                        if event.code == 4 and event.type == 3:
                            CONTROL_BYTE = [8, 9]
                            ZERO = 127.5
                            DEAD_ZONE = 27
                            SCALE = ZERO / (ZERO - DEAD_ZONE)
                            power = int((event.value - ZERO))
                            if sign(power) > 0:
                                direction = 0
                            else:
                                direction = 1
                            power = abs(power)

                            if power < DEAD_ZONE and self.left_motor[0] == 0 and self.right_motor[0] == 0:
                                continue
                            if power < DEAD_ZONE:
                                self.left_motor[0] = 0
                                self.right_motor[0] = 0
                                self.hlf_motor[0] = 0
                                self.hrf_motor[0] = 0
                                self.hlb_motor[0] = 0
                                self.hrb_motor[0] = 0
                            else:
                                power = int((power - DEAD_ZONE) * SCALE /
                                            self.mainContainer.controllerState.value('gearMode'))

                                if direction == 0:
                                    self.left_motor[0] = power
                                    self.right_motor[0] = power
                                    self.hlf_motor[0] = -power
                                    self.hrf_motor[0] = -power
                                    self.hlb_motor[0] = power
                                    self.hrb_motor[0] = power
                                else:
                                    self.left_motor[0] = -power
                                    self.right_motor[0] = -power
                                    self.hlf_motor[0] = power
                                    self.hrf_motor[0] = power
                                    self.hlb_motor[0] = -power
                                    self.hrb_motor[0] = -power

                        # if event.code == 308 and event.type == 1 and event.value == 1:
                        #     old = self.mainContainer.videoContainer.getVideoConfig()
                        #     print(old)
                        #     print(repr([old[1], old[0]]))
                        #     # self.mainContainer.videoContainer.setVideoConfig(
                        #     #     repr([[old[0], 5555, (old[2][1], old[2][0])]]))
                        #     self.mainContainer.videoContainer.setVideoConfig(repr([
                        #         [old[0][0], old[0][1], (old[0][2][1], old[0][2][0])],
                        #         [old[1][0], old[1][1], (old[1][2][1], old[1][2][0])],
                        #     ]))

                        # if event.code == 304 and event.type == 1 and event.value == 1:
                        #     self.rumbling()
                        # print(self.control_buffer[7])

                    left_direction = sign(self.left_motor[0] + self.left_motor[1])
                    if left_direction < 0:
                        left_direction = 1
                    else:
                        left_direction = 0
                    right_direction = sign(self.right_motor[0] + self.right_motor[1])
                    if right_direction < 0:
                        right_direction = 1
                    else:
                        right_direction = 0
# проверяем курс на соотвествие автокурсу, пока просто такс
                    if AUTO_HEAD == 1:
                        pid_yaw.auto_mode = True   # pid is enabled again
                        if yaw > 180:
                            if math.fabs(AUTOHEAD-yaw) > 180:
                                yaw_pid = yaw - 360
                            else:
                                yaw_pid = yaw
                        else:
                            if math.fabs(AUTOHEAD - yaw) > 180:
                                yaw_pid = yaw - 360
                            else:
                                yaw_pid = yaw
                            
                        pid_yaw.setpoint = AUTOHEAD*0.5
                        pid_yaw.output_limits = (-100, 100)
                        output_yaw = int(round(pid_yaw(yaw_pid*0.5) , 0))
                        output_yaw =  output_yaw
                        yaw = int(yaw)
                        print('AUTOHEAD   :', AUTOHEAD)
                        print('YAW        :', yaw)
                        print('YAW_PID    :', yaw_pid)
                        print('YAW_PID_OUT:', output_yaw)
                        if output_yaw < 0:
                            direction_yaw = 0
                            output_yaw = output_yaw * (-1)
                            self.hlf_motor[1] = output_yaw
                            self.hrf_motor[1] = -output_yaw
                            self.hlb_motor[1] = -output_yaw
                            self.hrb_motor[1] = output_yaw
                        else:
                            direction_yaw = 1
                            self.hlf_motor[1] = -output_yaw
                            self.hrf_motor[1] = output_yaw
                            self.hlb_motor[1] = output_yaw
                            self.hrb_motor[1] = -output_yaw
#                        print('KUUUUUUUUUUUUUUUUUUUUUUURS', AUTOHEAD)
                        






                    hlf_direction = sign(self.hlf_motor[0] + self.hlf_motor[1] + self.hlf_motor[2])
                    if hlf_direction < 0:
                        hlf_direction = 1
                    else:
                        hlf_direction = 0
                    hrf_direction = sign(self.hrf_motor[0] + self.hrf_motor[1] + self.hrf_motor[2])
                    if hrf_direction < 0:
                        hrf_direction = 1
                    else:
                        hrf_direction = 0

                    hlb_direction = sign(self.hlb_motor[0] + self.hlb_motor[1] + self.hlb_motor[2])
                    if hlb_direction < 0:
                        hlb_direction = 1
                    else:
                        hlb_direction = 0
                    hrb_direction = sign(self.hrb_motor[0] + self.hrb_motor[1] + self.hrb_motor[2])
                    if hrb_direction < 0:
                        hrb_direction = 1
                    else:
                        hrb_direction = 0




                    left_motor = min(abs(self.left_motor[0] + self.left_motor[1]), 0x7f)
                    right_motor = min(abs(self.right_motor[0] + self.right_motor[1]), 0x7f)
                    depth=self.mainContainer.depth
                    yaw=self.mainContainer.compass_angle

                    hlf_motor = min(abs(self.hlf_motor[0] + self.hlf_motor[1] + self.hlf_motor[2]), 0x7f)
                    hrf_motor = min(abs(self.hrf_motor[0] + self.hrf_motor[1] + self.hrf_motor[2]), 0x7f)
                    hlb_motor = min(abs(self.hlb_motor[0] + self.hlb_motor[1] + self.hlb_motor[2]), 0x7f)
                    hrb_motor = min(abs(self.hrb_motor[0] + self.hrb_motor[1] + self.hrb_motor[2]), 0x7f)

                    if AUTO_DEPTH==1:
                                
                                pid.auto_mode = True   # pid is enabled again
                                pid.setpoint = AUTODEPTH * 10
                                pid.output_limits = (-127, 127)
                                output = int(round(pid(depth * 10) , 0))
                                output =  output
                                if output < 0:
                                    direction = 0
                                    output = output * (-1)
                                    output = 128 + output
                                else:
                                    direction = 1
                                self.control_buffer[7] = output
                                self.control_buffer[12]=self.control_buffer[7]

                    self.control_buffer[8] = hlf_direction << 7 | hlf_motor
                    self.control_buffer[9] = hrf_direction << 7 | hrf_motor
                    self.control_buffer[10]= hlb_direction << 7 | hlb_motor
                    self.control_buffer[11]= hrb_direction << 7 | hrb_motor
                    if last_controll_buffer != self.control_buffer:
                        p0 = (self.control_buffer[8] & 0x7f)
                        if self.control_buffer[8] >> 7 == 0: p0 *= -1
                        p1 = (self.control_buffer[9] & 0x7f)
                        if self.control_buffer[9] >> 7 == 0: p1 *= -1
                        p2 = (self.control_buffer[7] & 0x7f)
                        if self.control_buffer[7] >> 7 == 1: p2 *= -1
                        self.mainContainer.powerState.emit(p0, p1, p2)
                        self.mainContainer.controlTube.send_control(self.control_buffer)
                  except:
                      self.selector.unregister(self.gamepad0)
                      self.selector.unregister(self.gamepad1)
                      print('Lose First Controller')
                      self.gamepad0 = None

                      self.LookingForFirstController()
                      self.selector.register(self.gamepad0, selectors.EVENT_READ)

                      self.LookingForSecondController()
                      self.selector.register(self.gamepad1, selectors.EVENT_READ)
                      
                elif device.fileno() == list(self.selector.get_map().keys())[1]:
                  MAX_ZONE_STOP = 200
                  MIN_ZONE_STOP = 50
                  try:
                    for event in device.read():
                        
                        if event.code not in [3, 4, 1,0]:
                            print(event, device)

                        if  event.code == 3 and event.type == 3 and MIN_ZONE_STOP < event.value < MAX_ZONE_STOP:
                            #event.code == 4 and event.type == 3 and MIN_ZONE_STOP < event.value < MAX_ZONE_STOP or \
                            
                            self.control_buffer[13]&=0x33
                            if self.control_buffer[13] == 0:
                                self.mainContainer.controllerState.setValue('cutter', 0)
                                self.mainContainer.controllerStateSignal.emit('cutter')
                        if  event.code == 4 and event.type == 3 and MIN_ZONE_STOP < event.value < MAX_ZONE_STOP:
                            #event.code == 4 and event.type == 3 and MIN_ZONE_STOP < event.value < MAX_ZONE_STOP or \
                            
                            self.control_buffer[13]&=0x0f
                            if self.control_buffer[13] == 0:
                                self.mainContainer.controllerState.setValue('cutter', 0)
                                self.mainContainer.controllerStateSignal.emit('cutter')

                        if (event.code == 313 or event.code == 311) and event.type == 1 and event.value == 0:
                            self.control_buffer[13]&=12
                            if self.control_buffer[13] == 0:
                                self.mainContainer.controllerState.setValue('cutter', 0)
                                self.mainContainer.controllerStateSignal.emit('cutter')

                            

                        if event.code == 4 and event.type == 3 and event.value < MIN_ZONE_STOP:
                            self.control_buffer[13]|=0x20
                            self.mainContainer.controllerState.setValue('cutter', 5)
                            self.mainContainer.controllerStateSignal.emit('cutter')
                        if event.code == 4 and event.type == 3 and event.value > MAX_ZONE_STOP:
                            self.control_buffer[13]|=0x10
                            self.mainContainer.controllerState.setValue('cutter', 6)
                            self.mainContainer.controllerStateSignal.emit('cutter')
                        if event.code == 3 and event.type == 3 and event.value < MIN_ZONE_STOP:
                            self.control_buffer[13]|=0x04
                            self.mainContainer.controllerState.setValue('cutter', 3)
                            self.mainContainer.controllerStateSignal.emit('cutter')
                        if event.code == 3 and event.type == 3 and event.value > MAX_ZONE_STOP:
                            self.control_buffer[13]|=0x08
                            self.mainContainer.controllerState.setValue('cutter', 4)
                            self.mainContainer.controllerStateSignal.emit('cutter')
                        if event.code == 311 and event.type == 1 and event.value == 1:
                            self.control_buffer[13]|=0x01
                            self.mainContainer.controllerState.setValue('cutter', 1)
                            self.mainContainer.controllerStateSignal.emit('cutter')
                        if event.code == 313 and event.type == 1 and event.value == 1:
                            self.control_buffer[13]|=0x02
                            self.mainContainer.controllerState.setValue('cutter', 2)
                            self.mainContainer.controllerStateSignal.emit('cutter')

                        if event.code == 0 and event.type == 3 and MIN_ZONE_STOP < event.value < MAX_ZONE_STOP:
                            self.control_buffer[14]&=0x33
                            if self.control_buffer[14] == 0:
                                self.mainContainer.controllerState.setValue('catcher', 0)
                                self.mainContainer.controllerStateSignal.emit('catcher')


                        if event.code == 1 and event.type == 3 and MIN_ZONE_STOP < event.value < MAX_ZONE_STOP:
                            self.control_buffer[14]&=0x0f
                            if self.control_buffer[14] == 0:
                                self.mainContainer.controllerState.setValue('catcher', 0)
                                self.mainContainer.controllerStateSignal.emit('catcher')


                        
                        if  (event.code == 310 or event.code == 312) and event.type == 1 and event.value == 0:
                            self.control_buffer[14]&=60
                            if self.control_buffer[14] == 0:
                                self.mainContainer.controllerState.setValue('catcher', 0)
                                self.mainContainer.controllerStateSignal.emit('catcher')
                            

                        if event.code == 1 and event.type == 3 and event.value < MIN_ZONE_STOP:
                            self.control_buffer[14]|=0x10
                            self.mainContainer.controllerState.setValue('catcher', 5)
                            self.mainContainer.controllerStateSignal.emit('catcher')
                        if event.code == 1 and event.type == 3 and event.value > MAX_ZONE_STOP:
                            self.control_buffer[14]|=0x20
                            self.mainContainer.controllerState.setValue('catcher', 6)
                            self.mainContainer.controllerStateSignal.emit('catcher')
                        if event.code == 0 and event.type == 3 and event.value < MIN_ZONE_STOP:
                            self.control_buffer[14]|=0x04
                            self.mainContainer.controllerState.setValue('catcher', 3)
                            self.mainContainer.controllerStateSignal.emit('catcher')
                        if event.code == 0 and event.type == 3 and event.value > MAX_ZONE_STOP:
                            self.control_buffer[14]|=0x08
                            self.mainContainer.controllerState.setValue('catcher', 4)
                            self.mainContainer.controllerStateSignal.emit('catcher')
                        if event.code == 310 and event.type == 1 and event.value == 1:
                            self.control_buffer[14]|=0x01
                            self.mainContainer.controllerState.setValue('catcher', 1)
                            self.mainContainer.controllerStateSignal.emit('catcher')
                        if event.code == 312 and event.type == 1 and event.value == 1:
                            self.control_buffer[14]|=0x02
                            self.mainContainer.controllerState.setValue('catcher', 2)
                            self.mainContainer.controllerStateSignal.emit('catcher')
                  except:
                      self.selector.unregister(self.gamepad1)
                      print('Lose Second Controller')
                      self.gamepad1 = None

                      self.LookingForSecondController()
                      self.selector.register(self.gamepad1, selectors.EVENT_READ)

                # 'catcher': [0, (("Захват", "black", 0),
                #                 ("Захват открыть", "black", 1),
                #                 ("Захват закрыть", "black", 2),
                #                 ("Захват влево", "black", 3),
                #                 ("Захват вправо", "black", 4),
                #                 ("Захват вперёд", "black", 5),
                #                 ("Захват назад", "black", 6))]}
