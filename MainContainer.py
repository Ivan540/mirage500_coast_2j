import time

import cv2
import mss
import numpy as np
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import pyqtSignal, QObject, QThread
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QSlider, QLabel, QPlainTextEdit, QPushButton

from ClockWidget import ClockWidgetScene
from CompassWidgetScene import CompassWidgetScene
from ControllerClass import ControllerClass, ControllerState
from DataClass import DataClass
from RollPitchWidgetScene import RollPitchWidgetScene
from SerialPortClass import SerialPortClass
from VideoContainer import VideoContainer

import config

class MainContainer(QObject):
    # signals
    controllerStateSignal = pyqtSignal(str)
    altitudeSignal = pyqtSignal(int)
    changeDataSignal = pyqtSignal(str)
    compassAngle = pyqtSignal(int, int, int, int, int)
    compass_angle_signal = pyqtSignal(int, int, int, int, int)
    angle_signal = pyqtSignal(int)
    rollAngle = pyqtSignal(int)
    pitchAngle = pyqtSignal(int)
    powerState = pyqtSignal(int, int, int)

    def __init__(self, central_widget, videoLayout, window):
        super().__init__()

        # components
        
        self.videoLayout = videoLayout
        self.centralwidget = central_widget
        self.videoContainer = VideoContainer(self, server_ip=config.Main.ip)
        
        if config.Control.use: self.serialPortClassBoard = SerialPortClass(self, config.Control.output)
        if config.Sonar.use:   self.serialPortClassSonar = SerialPortClass(self, config.Sonar.output)
        
        self.dataClassR = DataClass(self, server_ip=config.Main.ip, server_port=config.RTube.port, 
                                    use_telemetry=config.Telemetry.use if config.Telemetry.tube == 'r' else False,
                                    use_sonar=    config.Sonar.use     if config.Sonar.tube     == 'r' else False,
                                    cameraIdxs=config.RTube.video, label = 'R')
        self.dataClassL = DataClass(self, server_ip=config.Main.ip, server_port=config.LTube.port, 
                                    use_telemetry=config.Telemetry.use if config.Telemetry.tube == 'l' else False,
                                    use_sonar=    config.Sonar.use     if config.Sonar.tube     == 'l' else False,
                                    cameraIdxs=config.LTube.video, label = 'L')
        
        self.controlTube = self.dataClassR if config.Control.tube == 'r' else self.dataClassL 
        self.controllerState = ControllerState()
        self.controllerClass = ControllerClass(self)

        # load Depth
        self.DATA_FILE_NAME = config.Depth.data_filename
        self.calibrationDepth = 0
        # Will be updated at `setDepth`
        self.depth = 0
        # Will be updated at `update_compass_angle`
        self.compass_angle = None

        try:
            with open(self.DATA_FILE_NAME) as f:
                self.calibrationDepth = float(f.readline())
        except:
            pass

        # load Cross Picture
        #_crossImgPixmap = QPixmap("cross.png")
        _crossImgPixmap = QPixmap("/home/rov/rovbuilder-control-stable/cross.png")
        _crossImgPixmap = _crossImgPixmap.scaled(32, 32, QtCore.Qt.KeepAspectRatio)
        crossImg = QLabel()
        crossImg.setPixmap(_crossImgPixmap)
        crossImg.setAlignment(QtCore.Qt.AlignCenter)
        crossImg.mouseReleaseEvent = window.closeEvent

        # set default text font
        font = QtGui.QFont()
        font.setFamily("Times")
        font.setPointSize(28)

        # add depth label
        self.alt0 = QLabel("Глубина")
        self.alt0.setFont(font)

        # add all controls text
        self.controllerStateLabel = {}
        for switchName in self.controllerState.getAllSwitchNames():
            self.controllerStateLabel[switchName] = \
                QLabel(self.controllerState.name(switchName))
            self.controllerStateLabel[switchName].setFont(font)
            self.controllerStateLabel[switchName].setStyleSheet("color: %s;" %
                                                             self.controllerState.color(switchName))
        # add slot of controls
        self.controllerStateSignal.connect(lambda name: (
            self.controllerStateLabel[name].setText(self.controllerState.name(name)),
            self.controllerStateLabel[name].setStyleSheet("color: %s;" %
                                                         self.controllerState.color(name))
        ))

        # add big text label
        self.info = QPlainTextEdit("sadkijsweoifjjjje esfsflkisj mflks fsefnmcsk ")
        self.info.setStyleSheet("border-width: 0px; border-top:1px solid")
        self.info.setFont(font)

        # add widgets
        compassWidget = CompassWidgetScene()
        rollPitchWidget = RollPitchWidgetScene()
        self.clockWidget = ClockWidgetScene()
        self.compassWidget = compassWidget

        # make layouts
        vl = QtWidgets.QVBoxLayout()
        vl.setContentsMargins(1, 1, 1, 1)

        vl_0 = QtWidgets.QHBoxLayout()
        vl_0.setContentsMargins(1, 1, 1, 1)

        vl_0_0 = QtWidgets.QVBoxLayout()
        vl_0_0.setContentsMargins(1, 1, 1, 1)
        vl_0_0.setObjectName("verticalLayout")
        vl_0_0.addWidget(crossImg, 1)
        vl_0_0.addWidget(self.clockWidget, 1)
        vl_0_0.addWidget(self.controllerStateLabel['gearMode'],1)
        vl_0_0.addWidget(self.controllerStateLabel['autoDepth'],1)
        vl_0_0.addWidget(self.controllerStateLabel['autoHeading'],1)
        vl_0_0.addWidget(self.controllerStateLabel['laser'],1)
        vl_0_0.addWidget(self.controllerStateLabel['options_left'],1)
        vl_0_0.addWidget(self.controllerStateLabel['options_right'],1)
        vl_0_0.addWidget(self.controllerStateLabel['cutter'],1)
        vl_0_0.addWidget(self.controllerStateLabel['catcher'],1)
        vl_0_0.addWidget(self.controllerStateLabel['joysticks'],1)
        #vl_0_0.addWidget(self.info, 7)
        vl_0.addLayout(vl_0_0, 1)

        vl_0.addWidget(self.videoContainer.videoScreens[1], 4)

        vl_0_1 = QtWidgets.QVBoxLayout()
        vl_0_1.setContentsMargins(1, 1, 1, 1)
        vl_0_1.setObjectName("verticalLayout")
        vl_0_1.addWidget(compassWidget, 1)
        vl_0_1.addWidget(rollPitchWidget, 1)

        vl_0_1_0 = QtWidgets.QVBoxLayout()
        vl_0_1_0.setContentsMargins(1, 1, 1, 1)
        vl_0_1_0.setObjectName("verticalLayout")

        vl_0_1_0_1 = QtWidgets.QVBoxLayout()
        vl_0_1_0_1.setContentsMargins(1, 1, 1, 1)
        vl_0_1_0_1.setObjectName("verticalLayout")
        vl_0_1_0_1.addWidget(self.alt0, 1)
        button_depth_cal = QPushButton("Установка нуля Д.Д.")
        button_depth_cal.setFont(QtGui.QFont("Times", 16, QtGui.QFont.Bold))
        vl_0_1_0_1.addWidget(button_depth_cal, 1)

        vl_0_1_0.addLayout(vl_0_1_0_1)

        hl_0_1_0_0 = QtWidgets.QHBoxLayout()
        hl_0_1_0_0.setContentsMargins(1, 1, 1, 1)
        hl_0_1_0_0.setObjectName("horizontalLayout")
        self.sl = (QSlider(), QSlider(), QSlider())
        for i in range(3):
            self.sl[i].setMinimum(-127)
            self.sl[i].setMaximum(127)
            self.sl[i].setValue(0)
            self.sl[i].setTickInterval(1)
            hl_0_1_0_0.addWidget(self.sl[i])

        vl_0_1_0.addLayout(hl_0_1_0_0, 2)

        vl_0_1.addLayout(vl_0_1_0, 1)

        vl_0.addLayout(vl_0_1, 1)

        vl.addLayout(vl_0, 2)

        vl_1 = QtWidgets.QHBoxLayout()
        vl_1.setContentsMargins(1, 1, 1, 1)
        vl_1.addWidget(self.videoContainer.videoScreens[0])
        vl_1.addWidget(self.videoContainer.videoScreens[3])
        vl_1.addWidget(self.videoContainer.videoScreens[2])
        vl.addLayout(vl_1, 1)

        videoLayout.addLayout(vl)

        # start screen capturing
        self.cap = ScreenCapture(self, central_widget)
        if not self.cap.isRunning():
            self.cap.start()

        # add slot of components
        # Subscribe to the signal streams
        self.altitudeSignal.connect(self.setDepth)
        self.compassAngle.connect(compassWidget.setAngle)
        self.compass_angle_signal.connect(compassWidget.setAngle)
##        self.angle_signal.connect(compassWidget.setAngle)
        self.compass_angle_signal.connect(self.update_compass_angle)
##        self.angle_signal.connect(self.update_compass_angle)
        self.pitchAngle.connect(rollPitchWidget.setPitch)
        self.rollAngle.connect(rollPitchWidget.setRoll)
        self.powerState.connect(self.setPower)
        button_depth_cal.clicked.connect(self.depthCalibration)

    def update_compass_angle(self, angle):
        """Updates `compass_angle` field"""
        self.compass_angle = self.compassWidget.angle

    def setDepth(self, depth):
        """Updates `depth` field"""
        DEPTH_KOEF = 40
        self.depth = depth / DEPTH_KOEF - self.calibrationDepth
        self.alt0.setText("Глубина = %.1f м." % self.depth)

    def depthCalibration(self):
        try:
            self.calibrationDepth += self.depth
            with open(self.DATA_FILE_NAME, "w") as f:
                f.writelines(str(self.calibrationDepth))
        except:
            pass
        self.alt0.setText("Глубина = %.1f м." % self.depth)

    def setPower(self, p0, p1, p2):
        self.sl[0].setValue(p0)
        self.sl[1].setValue(p1)
        self.sl[2].setValue(p2)

    def off(self):
        self.dataClassL.off()
        self.dataClassR.off()
    
    def stop(self):
        try:
            self.cap.out = None
        except:
            pass
        try:
            self.clockWidget.stop()
            del self.clockWidget
        except:
            pass
        try:
            self.videoContainer.stop()
            del self.videoContainer
        except:
            pass
        try:
            self.dataClass0.stop()
            del self.dataClass
        except:
            pass
        try:
            self.dataClass1.stop()
            del self.dataClass
        except:
            pass
        try:
            self.controllerClass.stop()
            del self.controllerClass
        except:
            pass


class ScreenCapture(QThread):
    def __init__(self, parent, centralwidget):
        super().__init__(parent=parent)

        self.centralwidget = centralwidget

        self.out = 0

    def run(self):
        time.sleep(1)
        print('Video capture is started', config.Main.video_path)

        with mss.mss() as sct:
            monitor = {"top": 0, "left": 0, "width": 1920, "height": 1080}
            fourcc = cv2.VideoWriter_fourcc(*'MJPG')
            fps = 25.0
            splitTime = 10*60
            while self.out is not None:

                self.out = cv2.VideoWriter(config.Main.video_path + time.strftime("%Y-%m-%d_%H-%M-%S", time.localtime()) + ".avi",
                                           fourcc, fps,
                                           (1920, 1080))
                startTime = time.time()
                while time.time() - startTime <= splitTime:
                    last_time = time.time()
                    img = np.array(sct.grab(monitor))
                    frame = cv2.cvtColor(img, cv2.COLOR_RGBA2RGB)
                    # cv2.putText(frame, "FPS: %f" % (1.0 / (time.time() - last_time)),
                    #             (10, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    if self.out is not None:
                        self.out.write(frame)
                    t = 1.0 / fps - (time.time() - last_time)
                    if t > 0:
                        time.sleep(t)
                if self.out is not None:
                    self.out.release()

    def qimage_to_numpy(self, image):
        # Convert a QImage to a numpy array
        image = image.convertToFormat(QtGui.QImage.Format_RGB888)
        width = image.width()
        height = image.height()
        ptr = image.constBits()
        img = np.frombuffer(ptr.asstring(image.byteCount()), dtype=np.uint8).reshape(height, width, 3)
        return cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
