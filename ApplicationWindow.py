from PyQt5 import QtWidgets

import design
from MainContainer import MainContainer
import DataClass
import time


class ApplicationWindow(QtWidgets.QMainWindow, design.Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.mainContainer = MainContainer(self.centralwidget, self.videoLayout, self)

    def closeEvent(self, event):
        self.mainContainer.off()
        time.sleep(1)
        self.mainContainer.stop()
        event.accept()
        self.close()
