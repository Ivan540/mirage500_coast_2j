import ast

from VideoWidget import VideoWidget

import config

#resolutions = [(320,240),(640,480),(800,600),(1280,720),(1920,1080)]

cameras = ['Left', 'Forward', 'Right', 'Back']
camerasind = dict([(c[0], i) for i, c in enumerate(cameras)])

class VideoContainer:
    def __init__(self, main_container, server_ip='127.0.0.1'):
        self.mainContainer = main_container

        self.videoScreens = list()
        for c in cameras:
            vconf = getattr(config, 'Video'+c)
            vw = VideoWidget(serverIP=server_ip, serverPort=vconf.port, picSize=vconf.resolution,
                                         parent=self.mainContainer.centralwidget, label=c[0])
            self.videoScreens.append(vw)

        #self.videoScreens = [VideoWidget(serverIP=server_ip, serverPort=config.VideoLeft.port, picSize=config.VideoLeft.resolution,
        #                                 parent=self.mainContainer.centralwidget),
        #                     VideoWidget(serverIP=server_ip, serverPort=config.VideoForward.port, picSize=config.VideoForward.resolution,
        #                                 parent=self.mainContainer.centralwidget),
        #                     VideoWidget(serverIP=server_ip, serverPort=config.VideoRight.port, picSize=config.VideoRight.resolution,
        #                                 parent=self.mainContainer.centralwidget),
        #                     VideoWidget(serverIP=server_ip, serverPort=config.VideoBack.port, picSize=config.VideoBack.resolution,
        #                                 parent=self.mainContainer.centralwidget),
        #                     ]

    def stop(self):
        for el in self.videoScreens:
            el.stop()

    def getVideoConfig(self, camerasD=[]):
        return [self.videoScreens[camerasind[c.upper()]].getConfig() for c in camerasD]

    def setVideoConfig(self, data, camerasD=[]):
        try:
            data = ast.literal_eval(data)
            # if len(data) != len(self.videoScreens):
            if len(data) != len(camerasD):
                raise Exception
        except Exception as e:
            print(e)
        for i in range(len(data)):
            self.videoScreens[cameraIdxs[i]].changeConfig(data[i])
        raise Exception
        # self.mainContainer.dataClass.send(repr(self.getVideoConfig()), CONFIG_DATA)
        # print(repr(self.getVideoConfig()[0]))
        # self.mainContainer.dataClass0.send(repr(self.getVideoConfig()[0]), CONFIG_DATA)
        # self.mainContainer.dataClass1.send(repr(self.getVideoConfig()[1]), CONFIG_DATA)
