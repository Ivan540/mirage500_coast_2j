import configparser

comma_split = lambda s: [w.lstrip(' ').rstrip(' ') for w in s.split(',')]

class ConfDictWrapper:
    def __init__(self, d):
        self._d = d
        
    def __getattr__(self, k):
        try:
            t = self._d[k]
        except KeyError:
            return None
        else:
            if type(t) == dict:
                return DictWrapper(t)
            elif type(t) == str:
                if t.isnumeric():
                    return int(t)
                elif t.startswith("\"") and t.endswith("\""):
                    return t[1:-1]
                elif ',' in t:
                    if all(map(str.isnumeric, comma_split(t))):
                        return list(map(int, comma_split(t)))
                    elif all(map(str.isalpha, comma_split(t))):
                        return comma_split(t)
            return t


class ConfWrapper:
    def __init__(self, confname):
        self.config = configparser.ConfigParser()
        self.config.read(confname)
    
    def __getattr__(self, key):
        return ConfDictWrapper(self.config[key])


import sys
import os.path
sys.modules[__name__] = ConfWrapper(os.path.join(os.path.dirname(sys.modules[__name__].__file__), __name__+'.ini'))
