import time
from threading import Lock

import cv2

import socket
import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtCore import QSize, QThread, pyqtSignal, pyqtSlot, Qt
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QSizePolicy

import config

CHUNK_SIZE = 1024

class VideoWidget(QWidget):
    camId = 0

    def __init__(self, serverIP='127.0.0.1', serverPort=7777,
                 picSize=(800, 600), parent=None, label = '0'):
        QWidget.__init__(self, parent=parent)

        lay = QVBoxLayout(self)
        lay.setContentsMargins(0,0,0,0)
        self.videoScreenPixmap = QtWidgets.QLabel(parent)
        self.videoScreenPixmap.setText("")
        self.videoScreenPixmap.setObjectName("videoScreenPixmap")
        self.videoScreenPixmap.setMinimumSize(QSize(1, 1))

        self.videoScreenPixmap.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.videoScreenPixmap.setAlignment(Qt.AlignCenter)
        lay.addWidget(self.videoScreenPixmap)

        self.video = VideoThread(self, serverIP=serverIP, serverPort=serverPort,
                                 picSize=picSize, label = label)
        self.video.changePixmap.connect(self.setImage)
        self.video.start()

    def getConfig(self):
        return [self.video.serverIP, self.video.serverPort, self.video.picSize]

    def changeConfig(self, config):
        self.video.peer.close()
        self.video.changingConfigMutex.acquire()
        try:
            self.video.serverIP = config[0]
            self.video.serverPort = config[1]
            self.video.picSize = config[2]
        finally:
            self.video.changingConfigMutex.release()

    def stop(self):
        self.video.stop()

    @pyqtSlot(np.ndarray)
    def setImage(self, rgbImage):
        convertToQtFormat = QImage(rgbImage.data, rgbImage.shape[1], rgbImage.shape[0],
                                   QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(convertToQtFormat.scaled(self.videoScreenPixmap.width(),
                                                            self.videoScreenPixmap.height(),
                                                            Qt.KeepAspectRatio))
        self.videoScreenPixmap.setPixmap(pixmap)


class VideoThread(QThread):
    changePixmap = pyqtSignal(np.ndarray)

    def __init__(self, parent=None, serverIP='127.0.0.1', serverPort=7777,
                 picSize=(800, 600), label='0'):
        super().__init__(parent=parent)

        self.changingConfigMutex = Lock()
        self.peer = None

        self.buffForDataLength = 7

        self.stopped = False
        self.serverIP = serverIP
        self.serverPort = serverPort
        self.picSize = picSize
        self.connected = False
        self.exist_img = False
        self.label = label
        
        self.testing = bool(config.Main.testing)
        self.testing_text = label + ' ' + str(serverPort)

    def stop(self):
        self.stopped = True
        self.connected = False
        if self.peer:
            self.peer.close()
    
    def reconnect(self):
        self.connected = False
        if self.peer:
            self.clean_text()
            self.add_text('losed connection')
            self.peer.shutdown(1)
            self.peer.close()
        else:
            self.clean_img()
            self.clean_text()
            self.add_text('connecting', size=3)
        print('going to connect video', self.serverPort)
        while not self.connected:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                try:
                    s.bind((self.serverIP, self.serverPort))
                    s.listen()
                    s.settimeout(7.0)
                    self.peer, self.addr = s.accept()
                except (socket.timeout, OSError, cv2.error, MemoryError, ValueError) as e:
                    print('connection of video {0} failed with error {1} {2}, trying again'.format(self.serverPort, type(e), e))
                    self.clean_text()
                    error_text = str(type(e)).split("'")[1]
                    self.add_text(error_text, color=(255, 0, 0))
                    self.add_text('reconnecting')
                    #if type(e) == OSError:
                    time.sleep(0.2)
                    continue
                else:
                    self.peer.settimeout(5.0)
                    self.connected = True
                    print(self.serverIP, self.serverPort, 'connected')

    def add_text(self, text, size=1, color=(0, 255, 0), pos=None):
        font = cv2.FONT_HERSHEY_SIMPLEX
        if pos:
            tp = pos
        else:
            self.text_y += 30
            tp = (30, self.text_y)
        cv2.putText(self.text_img, str(text), tp, font, size, color, 2, cv2.LINE_AA)
        self.changePixmap.emit(self.text_img)

    def clean_img(self):
        if not self.exist_img:
            self.img = np.zeros((self.picSize[1], self.picSize[0], 3), np.uint8)
            self.exist_img = True
        font = cv2.FONT_HERSHEY_SIMPLEX
        cv2.putText(self.img, self.label, (300, 300), font, 5, (0, 255, 255), 6, cv2.LINE_AA)
        self.changePixmap.emit(self.img)
    
    def clean_text(self):
        self.text_y = self.picSize[1] - 120
        self.text_img = self.img.copy()
    
    def update_video(self, imgbytes):
        self.img = cv2.imdecode(np.fromstring(imgbytes, dtype=np.uint8),cv2.IMREAD_COLOR)
        self.img = np.fromstring(self.img, dtype=np.uint8).reshape(self.picSize[1],
                                                                   self.picSize[0], 3)
        self.img = cv2.cvtColor(self.img,cv2.COLOR_BGR2RGB)
        if self.testing: cv2.putText(self.img, self.testing_text, (30, 30),
                                     cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2, cv2.LINE_AA)
        self.changePixmap.emit(self.img)
 
    def run(self):
        while not self.stopped:
            if not self.connected: self.reconnect()
            self.changingConfigMutex.acquire()
            l = 0
            data = b''
            try:
                while not self.stopped:
                    lenbuff = data[:self.buffForDataLength]
                    l -= len(lenbuff)
                    data = data[self.buffForDataLength:]
                    while len(lenbuff) < self.buffForDataLength:
                        lenbuff += self.peer.recv(1)
                    msgLen = int.from_bytes(lenbuff, 'big')
                    while l < msgLen:
                        newData = self.peer.recv(CHUNK_SIZE)
                        if not newData: raise ConnectionRefusedError
                        l += len(newData)
                        data += newData
                    thisData = data[:msgLen]
                    data = data[msgLen:]
                    l -= msgLen
                    if len(thisData) == msgLen: self.update_video(thisData)
            except (socket.timeout, ConnectionRefusedError, cv2.error, MemoryError, ValueError) as e:
                print('losed connection with camera {0} due to error {1} {2}'.format(self.serverPort, type(e), e))
                if type(e) == MemoryError: print('MEMORY ERROR')
                self.connected = False
                #self.add_text('losed connection', color=(255, 0, 0))
                #error_text = str(type(e)).split("'")[1]
                #self.add_text(error_text, color=(255, 0, 0))
                #time.sleep(2)
            finally:
                    self.changingConfigMutex.release()

