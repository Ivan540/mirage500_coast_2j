#
import time

import bluetooth
import evdev
import threading

if __name__=="__main__":
    try:
        s = bluetooth.BluetoothSocket(bluetooth.L2CAP)
        s.connect(('A4:15:66:D5:6E:8D', 1))
        print("+")
        print(evdev.list_devices())
        # gamepad = evdev.InputDevice(evdev.list_devices()[0])
        gamepad = evdev.InputDevice(s)


        for event in gamepad.read_loop():
            if event.code not in [0, 3]:
                print(event)
    except bluetooth.btcommon.BluetoothError as err:
        print(err)

# A4:15:66:D5:6E:8D
# import bluetooth
#
# if __name__ == "__main__":
#     try:
#         s = bluetooth.BluetoothSocket(bluetooth.L2CAP)
#         s.connect(('A4:15:66:D5:6E:8D', 1))
#         print("+")
#         while True:
#             pass
#     except bluetooth.btcommon.BluetoothError as err:
#         print(err)
