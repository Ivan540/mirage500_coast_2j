import random
import socket
import time
from threading import Thread


class DataTxThread(Thread):
    def __init__(self, serverIP='127.0.0.1', serverPort=7888):
        super().__init__()

        self.stopped = False
        self.serverIP = serverIP
        self.serverPort = serverPort
        self.buffForDataLength = 3
        self.firstByte = b'_'

    def stop(self):
        self.stopped = True

    def run(self):
        while True:
            if self.stopped:
                break
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.connect((self.serverIP, self.serverPort))
                    print('Connected Tx to', (self.serverIP, self.serverPort))
                    while True:
                        msg = str(time.time())
                        s.sendall(self.firstByte)
                        s.send(len(msg).to_bytes(self.buffForDataLength, 'big'))
                        s.sendall(msg.encode())
                        time.sleep(1)
            except BrokenPipeError:
                time.sleep(1)
            except ConnectionRefusedError:
                time.sleep(1)
            except ConnectionResetError:
                time.sleep(1)
                print('robot is down')
