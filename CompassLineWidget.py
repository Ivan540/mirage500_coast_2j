from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import QWidget, QVBoxLayout


class CompassLineWidget(QWidget):
    angleChanged = pyqtSignal(int)

    def __init__(self, parent=None):

        QWidget.__init__(self, parent)
        self.setStyleSheet("background:transparent;")

        self._angle = 0
        self._margins = 0
        self._pointText = {0: "N", 45: "NE", 90: "E", 135: "SE", 180: "S",
                           225: "SW", 270: "W", 315: "NW"}

    def paintEvent(self, event):

        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.Antialiasing)

        painter.fillRect(event.rect(), self.palette().brush(QPalette.Window))
        self.drawMarkings(painter)

        painter.end()

    def drawMarkings(self, painter):

        painter.save()
        painter.translate(self.width() / 2, 0)
        scale = (self.width() - self._margins) / 400.0
        painter.scale(scale, scale)

        font = QFont(self.font())
        font.setPixelSize(10)
        metrics = QFontMetricsF(font)

        painter.setFont(font)

        painter.setPen(Qt.red)
        painter.drawLine(0, 5, 0, -5)

        painter.setPen(self.palette().color(QPalette.Shadow))

        step = 15
        angle = self._angle

        for i in range(angle, 179 + angle):
            x = i - angle
            if i % 45 == 0:
                painter.drawLine(x, 0, x, 15)
                idx = i % 360
                painter.drawText(x - metrics.width(self._pointText[idx]) / 2.0, 30,
                                 self._pointText[idx])
            elif i % step == 0:
                painter.drawLine(x, 0, x, 5)

        for i in range(-179 + angle, angle):
            x = i - angle
            if i % 45 == 0:
                painter.drawLine(x, 0, x, 15)
                idx = i % 360
                painter.drawText(x - metrics.width(self._pointText[idx]) / 2.0, 30,
                                 self._pointText[idx])
            elif i % step == 0:
                painter.drawLine(x, 0, x, 5)

        painter.restore()

    # def sizeHint(self):
    #
    #     return QSize(150, 150)

    def angle(self):
        return self._angle

    @pyqtSlot(int)
    def setAngle(self, angle):
        # print(angle)

        if angle != self._angle:
            self._angle = angle
            self.angleChanged.emit(angle)
            self.update()

    angle = pyqtProperty(float, angle, setAngle)
