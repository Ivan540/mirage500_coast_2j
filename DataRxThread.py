import socket
from threading import Thread

from PyQt5.QtCore import pyqtSignal, QThread

from DataTxThread import DataTxThread


class DataRxThread(QThread):
    changeRxData = pyqtSignal(str)

    def __init__(self, mainContainer, serverIP='127.0.0.1', serverPort=7878):
        super().__init__()

        self.stopped = False
        self.serverIP = serverIP
        self.serverPort = serverPort
        self.buffForDataLength = 3
        self.firstByte = b'_'
        self.mainContainer = mainContainer


    def stop(self):
        self.stopped = True

    def run(self):
        while True:
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                s.bind((self.serverIP, self.serverPort))
                s.listen()
                try:
                    conn, addr = s.accept()
                    with conn:
                        self.mainContainer.videoContainer.setVideoConfig("8")
                        print('Connected Rx by', addr)
                        while True:
                            if self.stopped:
                                break
                            newData = conn.recv(len(self.firstByte))
                            if newData == self.firstByte:
                                dataName = conn.recv(1)
                                msgLength = int.from_bytes(conn.recv(self.buffForDataLength), 'big')
                                data = conn.recv(msgLength).decode()
                                self.changeRxData.emit(data)
                            elif not newData:
                                raise ConnectionRefusedError
                except ConnectionRefusedError:
                    conn.close()

