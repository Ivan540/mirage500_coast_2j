from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QWidget, QGraphicsScene, QGraphicsView, QVBoxLayout

from CompassLineWidget import CompassLineWidget
from CompassWidget import CompassWidget


class SceneWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.com = CompassLineWidget()
        self.circom = CompassWidget()

        self.scene = QGraphicsScene()
        self.scene.addWidget(self.com)
        self.scene.addWidget(self.circom)

        self.view = QGraphicsView(self.scene)
        lay = QVBoxLayout(self)
        lay.addWidget(self.view)
        self.setLayout(lay)

    def paintEvent(self, event):
        self.com.setFixedSize(self.view.viewport().width(), self.view.viewport().height())
        self.circom.setFixedSize(self.view.viewport().width(), self.view.viewport().height())
        self.scene.setSceneRect(0,0,self.view.viewport().width(), self.view.viewport().height())
