import ast
import socket
import time
from threading import Thread, Event, Lock, Timer

ROW_DATA = b"0"
CONFIG_DATA = b"1"
CONTROL_DATA = b"2"
TELEMETRY_DATA = b"3"
POWEROFF_DATA = b"4"
SONAR_DATA = b"5"


class DataClass:
    def __init__(self, main_container, server_ip='127.0.0.1', server_port=7878, cameraIdxs=[], use_telemetry=False, use_sonar=False, label='0'):

        self.mainContainer = main_container
        self.serverIP = server_ip
        self.serverPort = server_port
        self.cameraIdxs = cameraIdxs

        self.stopped = False
        self.buffForDataLength = 3
        self.firstByte = b'_'
        self.msgTypeLength = 1
        self.peer = None
        self.connected = Event()
        self.sendMutex = Lock()
        
        self.label = label
        self.use_telemetry = bool(use_telemetry)
        self.use_sonar = bool(use_sonar)
        if use_sonar:
            self.sendSonarThread = Thread(target=self.sendSonarLoop)
            self.sendSonarThread.start()

        self.receiveThread = Thread(target=self.receiveLoop)
        self.sendThread = Thread(target=self.sendLoop)
        self.receiveThread.start()
        self.sendThread.start()

        # Timer(5, self.test).start()

    # def test(self):
    #     self.mainContainer.videoContainer.setVideoConfig(repr([['127.0.0.1', 7774, (400, 100)]]))

    def stop(self):
        self.stopped = True
        self.connected.set()
        if self.peer:
            self.peer.close()
    
    def off(self):
        self.send("1", POWEROFF_DATA)

    def send(self, msg, msgType, bytes=False):
        if not self.connected.isSet() or self.stopped:
            return False
        self.sendMutex.acquire()
        try:
            self.peer.sendall(self.firstByte)
            self.peer.sendall(msgType)
            self.peer.send(len(msg).to_bytes(self.buffForDataLength, 'big'))
            self.peer.sendall(msg if bytes else msg.encode())
        except (AttributeError, BrokenPipeError, ConnectionRefusedError, ConnectionResetError, OSError) as e:
            self.connected.clear()
            return False
        finally:
            self.sendMutex.release()
        return True

    def send_control(self, control_buffer):
        return self.send(repr(control_buffer), CONTROL_DATA)

    def sendSonarLoop(self):
        while not self.stopped:
            self.connected.wait()
            while self.peer:
                try:
                    data = self.mainContainer.serialPortClassSonar.readSonar()
                    #print('data from sonar client:', data)
                    t = self.send(data, SONAR_DATA, bytes=True)
                    #print('client sonar data send', t)
                    if not t:
                        break
                except Exception as e:
                    print('sonar error', e)
                    pass

    def sendLoop(self):
        while not self.stopped:
            self.connected.wait()
            while self.peer:
                self.mainContainer.serialPortClassBoard.read()
                if not self.send_control(self.mainContainer.controllerClass.control_buffer):
                    break
                time.sleep(0.05)

    def receiveLoop(self):
        while not self.stopped:
            try:
                with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    s.bind((self.serverIP, self.serverPort))
                    s.listen()
                    print(self.label, 'connecting')
                    s.settimeout(5.0)
                    self.peer, addr = s.accept()
                    self.peer.settimeout(3.0)
                    print(self.label, 'connected', addr)
                    self.connected.set()
                    with self.peer:
                        videoconfig_send = False
                        while self.connected.isSet() and not videoconfig_send:
                            try:
                                # print(self.mainContainer.videoContainer.getVideoConfig(self.cameraIdxs))
                                # self.send(repr(self.mainContainer.videoContainer.getVideoConfig()), CONFIG_DATA)
                                self.send(repr(self.mainContainer.videoContainer.getVideoConfig(self.cameraIdxs)),
                                          CONFIG_DATA)
                            except:
                                print(self.label, 'video not started. Trying again')
                                time.sleep(0.2)
                            else:
                                videoconfig_send = True
                                print(self.label, 'video started')
                        while self.connected.isSet():
                            newData = self.peer.recv(len(self.firstByte))
##                            print(newData)
                            if newData == self.firstByte:
                                msgType = self.peer.recv(self.msgTypeLength)
##                                print(msgType)
                                msgLength = int.from_bytes(self.peer.recv(self.buffForDataLength), 'big')
                                rawdata = b''
                                while len(rawdata) < msgLength:
                                    rawdata += self.peer.recv(msgLength-len(rawdata))
                                data = rawdata if msgType == SONAR_DATA else rawdata.decode()
                                if msgType == TELEMETRY_DATA and self.use_telemetry:
                                    try:
                                        data = ast.literal_eval(data)
                                        # print(data)
                                        # print(data[3], data[27]<<8|data[28])
                                        # print([((a), i + 22) for i, a in enumerate(data[29:31])])
                                        # print(bytes(data[27].encode()), bytes(data[28].encode()))
                                        self.mainContainer.altitudeSignal.emit(data[5] << 8 | data[6])
                                        self.mainContainer.compass_angle_signal.emit(data[7] << 8 | data[8],
                                                                             data[9] << 8 | data[10],
                                                                             data[11] << 8 | data[12],
                                                                             data[29] << 8 | data[30],
                                                                             data[27] << 8 | data[28])
                                        self.mainContainer.pitchAngle.emit(data[27] << 8 | data[28])
                                        self.mainContainer.rollAngle.emit(data[29] << 8 | data[30])
                                        self.mainContainer.compassAngle.emit(data[7] << 8 | data[8],
                                                                             data[9] << 8 | data[10],
                                                                             data[11] << 8 | data[12],
                                                                             data[29] << 8 | data[30],
                                                                             data[27] << 8 | data[28])
                                        self.mainContainer.changeDataSignal.emit(str(data[27] << 8 | data[28]))
                                    except Exception as e:
                                        pass
##                                        print(e)
                                #if msgType == ROW_DATA:
                                 #       self.mainContainer.changeDataSignal.emit(data)

                                if msgType == SONAR_DATA and self.use_sonar:
                                    #print('data from sonar:', data)
                                    self.mainContainer.serialPortClassSonar.writeSonar(data)
                                if msgType == CONFIG_DATA:
                                    print(data, msgType)
                            elif not newData:
                                #print('no fucking new data')
                                raise ConnectionRefusedError
            except (BrokenPipeError, ConnectionRefusedError, ConnectionResetError, OSError, socket.timeout) as e:
                #if self.use_sonar: print(time.time(), type(e), e)
                #print('Sonar tube' if self.use_sonar else 'Telemtry tube')
                print(self.label, 'losed connection due to', type(e), e)
                if self.peer: self.peer.close()
                self.connected.clear()
                time.sleep(0.3)
