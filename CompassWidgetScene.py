import math
from time import sleep

from PyQt5 import QtGui, QtCore
from PyQt5.QtCore import pyqtSignal, pyqtSlot
from PyQt5.QtWidgets import QWidget, QGraphicsScene, QGraphicsView, QVBoxLayout, QLabel, QPushButton, QDialog

from CompassLineWidget import CompassLineWidget
from CompassWidget import CompassWidget


class CompassWidgetScene(QWidget):
    angleChanged = pyqtSignal(int, int, int, int, int)

    def __init__(self, parent=None):
        QWidget.__init__(self, parent)

        self.DATA_FILE_NAME = "data.inf"

        Z = [0xffff, 0]
        X = [0xffff, 0]
        Y = [0xffff, 0]
        try:
            with open(self.DATA_FILE_NAME) as f:
                (Z[0], Z[1], X[0], X[1], Y[0], Y[1]) = f.readline().split()
                Z[0] = int(Z[0])
                Z[1] = int(Z[1])
                X[0] = int(X[0])
                X[1] = int(X[1])
                Y[0] = int(Y[0])
                Y[1] = int(Y[1])
        except Exception:
            with open(self.DATA_FILE_NAME, "w") as f:
                f.writelines("%d %d %d %d %d %d" % (Z[0], Z[1], X[0], X[1], Y[0], Y[1]))
        self.smoothCount = 10
        self.smoothCurCount = 0
        self.isCalibration = 0
        self.angle = 0
        self.Z = Z
        self.X = X
        self.Y = Y
        self.newZ = 0
        self.newX = 0
        self.newY = 0
        self.circom = CompassWidget()
        self.data = QLabel('Курс = %03d\u00B0' % 0)
        self.data.setFont(QtGui.QFont("Times", 16, QtGui.QFont.Bold))
        self.data.setAlignment(QtCore.Qt.AlignHCenter)

        button = QPushButton("Калибровка")
        button.setFont(QtGui.QFont("Times", 16, QtGui.QFont.Bold))

        self.scene = QGraphicsScene()
        self.scene.addWidget(self.circom)

        self.view = QGraphicsView(self.scene)
        self.view.setStyleSheet("border-width: 0px; border-top:1px solid")
        lay = QVBoxLayout(self)
        lay.setContentsMargins(0, 0, 0, 0)
        lay.addWidget(self.view)
        lay.addWidget(self.data)
        lay.addWidget(button)
        self.setLayout(lay)

        self.dialogWindow = QDialog()
        font = QtGui.QFont()
        font.setFamily("Times")
        font.setPointSize(32)
        font.setItalic(True)
        self.dialogWindow.setStyleSheet("background-color: rgb(55, 179, 255)")
        self.dialogWindow.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.dialogWindow.setWindowModality(QtCore.Qt.ApplicationModal)

        dialog_lay = QVBoxLayout()

        self.text_calibration = QLabel("Калибровка компаса!!!\n"
                                       "Поверните ТНПА строго в горизонтальном положении на 360\u00B0.",
                                       self.dialogWindow)
        self.text_calibration.setFont(font)
        self.text_calibration.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.text_calibration.setStyleSheet("color: rgb(255, 0, 0);")
        dialog_lay.addWidget(self.text_calibration)

        self.button_next = QPushButton("Продолжить калибровку")
        self.button_next.setFont(QtGui.QFont("Times", 32, QtGui.QFont.Bold))
        dialog_lay.addWidget(self.button_next)
        self.button_stop = QPushButton("Закончить калибровку")
        self.button_stop.setFont(QtGui.QFont("Times", 32, QtGui.QFont.Bold))
        dialog_lay.addWidget(self.button_stop)

        self.dialogWindow.setLayout(dialog_lay)

        self.button_next.clicked.connect(self.nextCalibration)
        self.button_stop.clicked.connect(self.stopCalibration)

        button.clicked.connect(self.startCalibration)

    def startCalibration(self):
        self.isCalibration = 1
        self.Z = [0xffff, 0]
        self.X = [0xffff, 0]
        self.Y = [0xffff, 0]

        self.dialogWindow.exec_()
        self.isCalibration = 0
        self.text_calibration.setText("Калибровка компаса!!!\n"
                                      "Поверните ТНПА строго в горизонтальном положении на 360\u00B0.")
        self.button_next.show()

    def nextCalibration(self):
        self.isCalibration = 2
        self.text_calibration.setText("Калибровка компаса!!!\n"
                                      "Поверните ТНПА строго вокруг продольной оси аппарата.")
        self.button_next.hide()

    def stopCalibration(self):
        with open(self.DATA_FILE_NAME, "w") as f:
            f.writelines("%d %d %d %d %d %d" % (self.Z[0], self.Z[1], self.X[0], self.X[1], self.Y[0], self.Y[1]))
        self.dialogWindow.reject()

    def paintEvent(self, event):
        self.circom.setFixedSize(self.view.viewport().width(), self.view.viewport().height())
        self.scene.setSceneRect(0, 0, self.view.viewport().width(), self.view.viewport().height())

    @pyqtSlot(int, int, int, int, int)
    def setAngle(self, Z, X, Y, roll, pitch):
        angle = 0
        if self.isCalibration == 1:
            if X and Y:
                self.X = [min(self.X[0], X), max(self.X[1], X)]
                self.Y = [min(self.Y[0], Y), max(self.Y[1], Y)]
        if self.isCalibration == 2:
            if Z:
                self.Z = [min(self.Z[0], Z), max(self.Z[1], Z)]
        else:
            if self.smoothCurCount < self.smoothCount:
                self.newZ += Z
                self.newX += X
                self.newY += Y
                self.smoothCurCount += 1
            else:
                self.smoothCurCount = 0
            if self.smoothCurCount == 0:
                Z = self.newZ/self.smoothCount
                X = self.newX/self.smoothCount
                Y = self.newY/self.smoothCount
                self.newZ = 0
                self.newX = 0
                self.newY = 0
                try:
                    # print(Z, X, Y)
                    medZ = (self.Z[1] - self.Z[0]) / 2
                    medX = (self.X[1] - self.X[0]) / 2
                    medY = (self.Y[1] - self.Y[0]) / 2
                    newZ = Z - self.Z[0] - medZ
                    newX = X - self.X[0] - medX
                    newY = Y - self.Y[0] - medY
                    if medX > medY:
                        # if medX > medZ:
                        if True:
                            newY = newY * medX / medY
                            newZ = newZ * medX / medZ
                        else:
                            newX = newX * medZ / medX
                            newY = newY * medZ / medY
                    else:
                        # if medY > medZ:
                        if True:
                            newX = newX * medY / medX
                            newZ = newZ * medY / medZ
                        else:
                            newX = newX * medZ / medX
                            newY = newY * medZ / medY
                    roll *= math.pi / 180
                    pitch *= math.pi / 180
                    # newX = - newX * math.cos(roll) \
                    #        + newY * math.sin(pitch) * math.sin(roll) \
                    #        - newZ * math.sin(roll) * math.cos(pitch)
                    # newY = -newY * math.cos(pitch) + newZ * math.sin(pitch)
                    # print(newZ,newX,newY)
                    
                    angle = int(180 / math.pi * math.atan2(-newX, -newY) + 180)
                    self.angle = angle
                    self.mainContainer.angle_signal.emit(angle)
                except Exception as e:
                    pass
                self.circom.setAngle(angle)
                self.data.setText('Курс = %03d\u00B0' % angle)
